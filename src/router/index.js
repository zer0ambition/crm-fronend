import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    meta: {layout: 'empty'},
    component: () => import('../views/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: {layout: 'empty'},
    component: () => import('../views/Register.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    meta: {layout: 'main'},
    component: () => import('../views/Profile.vue')
  },
  {
    path: '/categories',
    name: 'categories',
    meta: {layout: 'main'},
    component: () => import('../views/Categories.vue')
  },
  {
    path: '/',
    name: 'home',
    meta: {layout: 'main'},
    component: () => import('../views/Home.vue')
  },
  {
    path: '/history',
    name: 'history',
    meta: {layout: 'main'},
    component: () => import('../views/History.vue')
  },
  {
    path: '/detail',
    name: 'detail',
    meta: {layout: 'main'},
    component: () => import('../views/Detail.vue')
  },
  {
    path: '/record',
    name: 'record',
    meta: {layout: 'main'},
    component: () => import('../views/Record.vue')
  },
  {
    path: '/planning',
    name: 'planning',
    meta: {layout: 'main'},
    component: () => import('../views/Planning.vue')
  },
  {
    path: '/leads',
    name: 'leads',
    meta: {layout: 'main'},
    component: () => import('../views/leads/Leads.vue')
  },
  {
    path: '/leads/:id',
    name: 'lead',
    meta: {layout: 'main'},
    component: () => import('../views/leads/LeadDetail.vue')
  },
  {
    path: '/leads-create',
    name: 'createLead',
    meta: {layout: 'main'},
    component: () => import('../components/leads/CreateLead.vue')
  },
  {
    path: '/pipelines',
    name: 'pipelines',
    meta: {layout: 'main'},
    component: () => import('../views/pipelines/Pipelines.vue')
  },
  {
    path: '/pipelines/:id',
    name: 'pipeline',
    meta: {layout: 'main'},
    component: () => import('../views/pipelines/PipelineDetail.vue')
  },
  {
    path: '/contacts',
    name: 'contacts',
    meta: {layout: 'main'},
    component: () => import('../views/contacts/Contacts.vue')
  },
  {
    path: '/contacts/:id',
    name: 'contact',
    meta: {layout: 'main'},
    component: () => import('../views/contacts/ContactDetail.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
