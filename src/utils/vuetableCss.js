export default {
    table: {
        tableWrapper: '',
        tableHeaderClass: 'mb-0',
        tableBodyClass: 'mb-0',
        tableClass: 'table table-bordered table-hover',
        loadingClass: 'loading',
        ascendingIcon: 'fa fa-chevron-up',
        descendingIcon: 'fa fa-chevron-down',
        ascendingClass: 'sorted-asc',
        descendingClass: 'sorted-desc',
        sortableIcon: 'fa fa-sort',
        detailRowClass: 'vuetable-detail-row',
        handleIcon: 'fa fa-bars text-secondary',
    },
    pagination: {
        wrapperClass: 'pagination right',
        activeClass: 'active teal lighten-2',
        disabledClass: 'disabled',
        pageClass: 'waves-effect',
        linkClass: 'waves-effect',
        paginationClass: 'pagination',
        paginationInfoClass: 'float-left',
        dropdownClass: 'form-control',
        icons: {
            first: 'material-icons',
            prev: 'material-icons',
            next: 'material-icons',
            last: 'material-icons',
        }
    }
}
