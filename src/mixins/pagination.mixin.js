export default {
    data() {
        return {
            pagination: {
                total: 0,
                page: 1,
                limit: 10,
                lastPage: 1
            },
            queryParams: {
                limit: 10,
                page: 1
            }
        }
    },
    methods: {
        async pageChangeHandler(page) {
            this.queryParams.page = page
            await this.fetchList()
        },
        setupPagination(res) {
            this.pagination = {total: res.data.total, page: res.data.page, lastPage: res.data.lastPage, limit: res.limit}
        }
    }
}
