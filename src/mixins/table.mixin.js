export default {
    methods: {
        transform(res) {
            return  {
                data: res.data.items,
                total: res.data.total,
                current_page: res.data.page,
                last_page: res.data.lastPage,
                per_page: res.data.limit,
                from: res.data.from,
                to: res.data.to,
                next_page_url: res.data.nextPageUrl,
                prev_page_url: res.data.prevPageUrl,
                url: res.data.url
            }
        },
        getQueryParams(sortOrder, currentPage, perPage) {
            return {
                sortBy: sortOrder[0].field,
                sortDirection: sortOrder[0].direction,
                page: currentPage,
                limit: perPage
            }
        },
        onPaginationData(paginationData) {
            this.$refs.pagination.setPaginationData(paginationData);
            this.$refs.paginationInfo.setPaginationData(paginationData);
        },
        onChangePage(page) {
            this.$refs.vuetable.changePage(page);
        },
        refreshTable() {
            this.$refs.vuetable.reload();
        }
    }
}
