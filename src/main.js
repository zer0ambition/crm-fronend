import Vue from 'vue'
import Vuelidate from "vuelidate/src";
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from "./filters/date.filter";
import './registerServiceWorker'
import messagePlugin from './utils/message.plugin'
import Paginate from 'vuejs-paginate'

Vue.config.productionTip = false

Vue.component('Paginate', Paginate)
Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
