import request from '@/utils/request'

export default {
    actions: {
        async fetchLeads(_, params) {
            return request({
                url: '/leads',
                method: 'get',
                params: params
            })
        },
        async fetchLeadById(_, id) {
            return request({
                url: `/leads/${id}`,
                method: 'get'
            })
        },
        async patchLeadById(_, data) {
            return request({
                url: `/leads/${data.id}`,
                method: 'patch',
                data: data
            })
        },
        async createLead(_, data) {
            return request({
                url: `/leads`,
                method: 'post',
                params: data
            })
        },
        async deleteLeadById(_, id) {
            return request({
                url: `/leads/${id}`,
                method: 'delete'
            })
        }
    },
    state: {
        leads: [],
        selectedPipeline: null,
        isKanban: true
    },
    mutations: {
        setSelectedPipeline(state, pipeline) {
            state.selectedPipeline = pipeline
        },
        setLeadsView(state, con) {
            state.isKanban = con
        }
    },
    getters: {
        selectedPipeline: state => state.selectedPipeline,
        isKanban: state => state.isKanban
    }
}
