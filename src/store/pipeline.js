import request from '@/utils/request'

export default {
    actions: {
        async fetchPipelines(_, params) {
            return request({
                url: '/pipelines',
                method: 'get',
                params: params
            })
        },
        async fetchPipelineById(_, id) {
            return request({
                url: `/pipelines/${id}`,
                method: 'get'
            })
        },
        async patchPipelineById(_, data) {
            return request({
                url: `/pipelines/${data.id}`,
                method: 'patch',
                data: data
            })
        },
        async createPipeline(_, data) {
            return request({
                url: `/pipelines`,
                method: 'post',
                params: data
            })
        },
        async deletePipelineById(_, id) {
            return request({
                url: `/pipelines/${id}`,
                method: 'delete'
            })
        }
    },
    state: {
        pipelines: []
    },
    mutations: {
        addPipeline(state, pipeline) {
            state.pipelines.push(pipeline)
        },
        deletePipeline(state, id) {
            const i = state.pipelines.filter((i) => i.id === id)
            state.pipelines.splice(state.pipelines.indexOf(i), 1)
        },
        setPipelines(state, pipelines) {
            state.pipelines = pipelines
        }
    },
    getters: {
        pipelines: state => {
            return state.pipelines
        }
    }
}
