import Vue from 'vue'
import Vuex from 'vuex'
import pipeline from "./pipeline";
import contact from "./contact";
import lead from "./lead";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    pipeline, contact, lead
  }
})
