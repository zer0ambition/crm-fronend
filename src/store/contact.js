import request from '@/utils/request'

export default {
    actions: {
        async fetchContacts(_, params) {
            return request({
                url: '/contacts',
                method: 'get',
                params: params
            })
        },
        async fetchContactById(_, id) {
            return request({
                url: `/contacts/${id}`,
                method: 'get'
            })
        },
        async patchContactById(_, data) {
            return request({
                url: `/contacts/${data.id}`,
                method: 'patch',
                params: data
            })
        },
        async createContact(_, data) {
            return request({
                url: `/contacts`,
                method: 'post',
                params: data
            })
        },
        async deleteContactById(_, id) {
            return request({
                url: `/contacts/${id}`,
                method: 'delete'
            })
        }
    },
    state: {
        contacts: []
    },
    mutations: {
        addContact(state, contact) {
            state.contacts.push(contact)
        },
        deleteContact(state, id) {
            const i = state.contacts.filter((i) => i.id === id)
            state.contacts.splice(state.contacts.indexOf(i), 1)
        },
        setContacts(state, contacts) {
            state.contacts = contacts
        }
    },
    getters: {
        contacts: state => {
            return state.contacts
        }
    }
}
